use directories::{BaseDirs, ProjectDirs, UserDirs};

use crate::error::Error;
use crate::logging::LoggingSetupBuilder;

/// Builder for [`CrateSetup`].
///
/// [`CrateSetup`]: ./struct.CrateSetup.html
#[derive(Default)]
pub struct CrateSetupBuilder {
    qualifier: Option<String>,
    company_name: Option<String>,
    application_name: Option<String>,
}

/// Basic data for a crate.
pub struct CrateSetup {
    application_name: String,
    base_dirs: BaseDirs,
    user_dirs: UserDirs,
    project_dirs: ProjectDirs,
}

impl CrateSetupBuilder {
    /// Create new `CrateSetupBuilder`.
    pub fn new() -> Self {
        Self::default()
    }

    /// Set application name.
    pub fn with_app_name(mut self, application_name: impl AsRef<str>) -> Self {
        self.application_name = Some(application_name.as_ref().to_string());
        self
    }

    /// Set fully qualified application name.
    pub fn with_full_app_name(
        mut self,
        qualifier: impl AsRef<str>,
        company_name: impl AsRef<str>,
        application_name: impl AsRef<str>,
    ) -> Self {
        self.qualifier = Some(qualifier.as_ref().to_string());
        self.company_name = Some(company_name.as_ref().to_string());
        self.application_name = Some(application_name.as_ref().to_string());
        self
    }

    /// Build `CrateSetup`.
    pub fn build(self) -> Result<CrateSetup, Error> {
        let application_name = self
            .application_name
            .unwrap_or_else(|| env!("CARGO_PKG_NAME").to_string());
        let base_dirs = BaseDirs::new().ok_or(Error::DirectoryBuildError)?;
        let user_dirs = UserDirs::new().ok_or(Error::DirectoryBuildError)?;
        let project_dirs = match (self.qualifier, self.company_name) {
            (Some(qualifier), Some(company_name)) => {
                ProjectDirs::from(&qualifier, &company_name, &application_name)
            }
            _ => ProjectDirs::from("", "", &application_name),
        }
        .ok_or(Error::DirectoryBuildError)?;

        Ok(CrateSetup {
            application_name,
            base_dirs,
            user_dirs,
            project_dirs,
        })
    }
}

impl CrateSetup {
    /// Get simple application name.
    pub fn application_name(&self) -> &str {
        &self.application_name
    }

    /// Get base directories.
    pub fn base_dirs(&self) -> &BaseDirs {
        &self.base_dirs
    }

    /// Get user directories.
    pub fn user_dirs(&self) -> &UserDirs {
        &self.user_dirs
    }

    /// Get project-specific directories.
    pub fn project_dirs(&self) -> &ProjectDirs {
        &self.project_dirs
    }

    /// Get logging setup builder.
    pub fn logging_setup(&self) -> LoggingSetupBuilder {
        LoggingSetupBuilder::new(self)
    }
}
