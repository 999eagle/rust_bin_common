use std::fmt;

/// Main error type.
#[derive(Debug)]
pub enum Error {
    /// Failed to setup directories.
    DirectoryBuildError,
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::DirectoryBuildError => write!(f, "Failed to setup directories"),
        }
    }
}
