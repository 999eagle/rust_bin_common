use std::{
    fs::{self, File},
    io::{self, Read, Write},
    path::PathBuf,
};

use log::{debug, error};
use xz2::write::XzEncoder;

pub fn get_old_log_files(log_dir: &PathBuf) -> Vec<PathBuf> {
    let log_files = fs::read_dir(log_dir).expect("Failed to read log dir");
    let mut files_to_compress = Vec::new();
    for entry in log_files {
        let path = entry.unwrap().path();
        if path.is_file()
            && path
                .extension()
                .map_or(false, |e| e.to_str().map_or(false, |s| s == "log"))
            && !path.ends_with("latest.log")
        {
            files_to_compress.push(path);
        }
    }
    files_to_compress
}

pub fn clean_up_old_logs(old_log_files: Vec<PathBuf>) {
    std::thread::spawn(|| {
        let mut buffer = [0; 4096];
        for file in old_log_files {
            let compressed_file_name = file.with_extension("log.xz");
            debug!(
                "Compressing old log file {:?} to {:?}",
                file, compressed_file_name
            );
            match File::create(&compressed_file_name) {
                Err(err) => {
                    error!(
                        "Error opening compressed file {:?}: {}",
                        compressed_file_name, err
                    );
                }
                Ok(file_write) => match File::open(&file) {
                    Err(err) => {
                        error!("Error opening log file {:?}: {}", file, err);
                    }
                    Ok(file_read) => match compress_log_file(file_read, file_write, &mut buffer) {
                        Ok(_) => {
                            if let Err(err) = fs::remove_file(&file) {
                                error!("Error removing log file {:?}: {}", file, err);
                            }
                        }
                        Err(CompressError::Read(err)) => {
                            error!("Error reading log file {:?}: {}", file, err)
                        }
                        Err(CompressError::Write(err)) => error!(
                            "Error writing compressed file {:?}: {}",
                            compressed_file_name, err
                        ),
                        Err(CompressError::WriteLength(_, _)) => error!(
                            "Not enough space writing compressed file {:?}",
                            compressed_file_name
                        ),
                    },
                },
            }
        }
    });
}

enum CompressError {
    Read(io::Error),
    Write(io::Error),
    WriteLength(usize, usize),
}

fn compress_log_file(
    mut file_read: File,
    file_write: File,
    buffer: &mut [u8],
) -> Result<(), CompressError> {
    let mut encoder = XzEncoder::new(file_write, 9);
    loop {
        let read = file_read.read(buffer).map_err(CompressError::Read)?;
        if read == 0 {
            return Ok(());
        }
        let written = encoder
            .write(&buffer[..read])
            .map_err(CompressError::Write)?;
        if read != written {
            return Err(CompressError::WriteLength(read, written));
        }
    }
}
