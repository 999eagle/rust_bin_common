pub use error::Error;
pub use setup::{CrateSetup, CrateSetupBuilder};

pub mod error;
pub mod logging;
mod setup;
