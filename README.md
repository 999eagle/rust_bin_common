# bin_common

A library for common things in binaries. Mainly useful for a quick logging setup with support for logging to files, logging panics and log rotation.

## Usage

This is an example for how to use this library.

```rust
let crate_setup = CrateSetupBuilder::new()
	.with_app_name("my-cool-app") // set app name for paths
	.build()
	.unwrap();
crate_setup
	.logging_setup()
	.with_verbosity(1) // log only INFO and above messages
	.with_log_to_file(true) // write all log messages to file and stdout
	.with_log_panics(true) // log panics
	.with_log_rotation(true) // rotate log files
	.build()
	.unwrap();
log::info("{} starting", crate_setup.application_name());
```

The `CrateSetup` stores the application name and resulting base directories according to the XDG directory spec and allows setting up logging.
